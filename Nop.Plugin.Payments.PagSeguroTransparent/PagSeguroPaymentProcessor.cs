﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using NopBrasil.Plugin.Payments.PagSeguroTransparent;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Models;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Task;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Validators;
using Uol.PagSeguro.Constants;
using Uol.PagSeguro.Domain;
using Uol.PagSeguro.Domain.Direct;
using Uol.PagSeguro.Service;
using PaymentMethodType = Nop.Services.Payments.PaymentMethodType;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent
{
    public class PagSeguroPaymentProcessor : BasePlugin, IPaymentMethod
    {
        private readonly ILogger _logger;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly PagSeguroTask _pagSeguroTask;
        private readonly PagSeguroPaymentSetting _pagSeguroPaymentSetting;
        private readonly ICustomerService _customerService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;

        public PagSeguroPaymentProcessor(ILogger logger, IWorkContext workContext, IOrderService orderService,
             ISettingService settingService, ICustomerService customerService,
            IWebHelper webHelper, PagSeguroTask pagSeguroTask, PagSeguroPaymentSetting pagSeguroPaymentSetting,
            ICustomerAttributeParser customerAttributeParser, ICustomerAttributeService customerAttributeService, ILocalizationService localizationService)
        {
            this._logger = logger;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._pagSeguroTask = pagSeguroTask;
            this._pagSeguroPaymentSetting = pagSeguroPaymentSetting;
            this._customerService = customerService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerAttributeService = customerAttributeService;
            this._orderService = orderService;
            this._workContext = workContext;
            this._localizationService = localizationService;
        }

        public override void Install()
        {
            var settings = new PagSeguroPaymentSetting
            {
                Timeout = 10000,
                PagSeguroUrl = "https://ws.sandbox.pagseguro.uol.com.br/v2",
                PagSeguroUrlSTC = "https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js",
                PagSeguroEmail = "",
                PagSeguroToken = "",
                MaxInstallmentsNumberInterestFree = 1,
                DescricaoBoleto = "Abrirá uma janela com o boleto para ser impresso ou salvo para que o pagamento possa ser efetuado",
                DescricaoDebito = "Abrirá uma nova janela com o site do seu banco para que você faça o pagamento"
            };
            _settingService.SaveSetting(settings);

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.EmailAdmin.PagSeguro", "Email - PagSeguro");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.EmailAdmin.PagSeguro.Hint", "Informe o email de sua conta no PagSeguro.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Token.PagSeguro", "Token - PagSeguro");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Token.PagSeguro.Hint", "Informe o email de sua conta no PagSeguro.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguro", "URL - PagSeguro");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguro.Hint", "Informe a URL de integração do PagSeguro.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguroSTC", "URL STC - PagSeguro");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguroSTC.Hint", "Informe a URL de integração STC do PagSeguro.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Timeout", "Timeout - PagSeguro");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Timeout.Hint", "Informe o tempo de espera máximo na integração com o PagSeguro.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.MaxInstallmentsNumber.InterestFree", "Número máximo de parcelas sem juros");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.MaxInstallmentsNumber.InterestFree.Hint", "Informe o número máximo de parcelas permitido sem cobrança de juros.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Boleto.Descricao", "Descrição do funcionamento do pagamento por boleto");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Boleto.Descricao.Hint", "Uma explicação para o cliente de como ele deverá proceder ao pagar com boleto.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Descricao", "Descrição do funcionamento do pagamento por débito em conta");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Descricao.Hint", "Uma explicação para o cliente de como ele deverá proceder ao pagar com débito em conta.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.CpfAttributeName", "Nome do atributo que representa o CPF/CNPJ do cliente");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.CpfAttributeName.Hint", "informe qual o nome do atributo que representa o CPF/CNPJ do cliente.");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Numero", "Número do cartão");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Validade", "Validade do cartão");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.NomeTitular", "Nome do titular");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.CVV", "Código de segurança");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.CpfTitular", "CPF do Titular");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Telefone", "Telefone do titular");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.DDD", "DDD");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.DataNascimento", "Data de nascimento");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.NumeroParcelas", "Número de parcelas");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Banco", "Banco");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento", "Tipo de pagamento");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento.CartaoCredito", "Cartão de crédito");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento.Debito", "Débito em conta");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento.Boleto", "Boleto bancário");

            _pagSeguroTask.InstallTask();

            base.Install();
        }

        public override void Uninstall()
        {
            _pagSeguroTask.UninstallTask();

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.EmailAdmin.PagSeguro");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.EmailAdmin.PagSeguro.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Token.PagSeguro");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Token.PagSeguro.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguro");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguro.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguroSTC");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguroSTC.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Timeout");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Timeout.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.MaxInstallmentsNumber.InterestFree");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.MaxInstallmentsNumber.InterestFree.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Boleto.Descricao");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Boleto.Descricao.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Descricao");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Descricao.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.CpfAttributeName");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.CpfAttributeName.Hint");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Numero");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Validade");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.NomeTitular");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.CVV");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.CpfTitular");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Telefone");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.DDD");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.DataNascimento");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.NumeroParcelas");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Banco");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento.CartaoCredito");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento.Debito");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento.Boleto");

            base.Uninstall();
        }

        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            //Método que irá fazer a chamada no PagSeguro para débito do cartão
            var processPaymentResult = new ProcessPaymentResult();
            processPaymentResult.NewPaymentStatus = PaymentStatus.Pending;

            Checkout checkout = new BoletoCheckout();
            checkout.PaymentMode = PaymentMode.DEFAULT;
            checkout.ReceiverEmail = _pagSeguroPaymentSetting.PagSeguroEmail;
            checkout.Currency = Currency.Brl;
            checkout.Reference = processPaymentRequest.OrderGuid.ToString();

            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            if (customer == null)
                throw new Exception("Customer cannot be loaded");

            checkout.Sender = new Sender(
                customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName,
                customer.Email,
                new Phone("", customer.BillingAddress.PhoneNumber)
            );
            checkout.Sender.Hash = processPaymentRequest.CustomValues["IdComprador"].ToString();

            var xmlAtts = customer.BillingAddress.CustomAttributes;
            var attributes = _customerAttributeService.GetAllCustomerAttributes();
            var cpfTitular = _customerAttributeParser.ParseValues(xmlAtts, attributes.Where(aa => aa.Name == _pagSeguroPaymentSetting.CpfAttributeName).FirstOrDefault().Id)[0];

            var senderCPF = new SenderDocument(Documents.GetDocumentByType("CPF"), cpfTitular);
            checkout.Sender.Documents.Add(senderCPF);

            //colocar os itens do pedido


            //colocar as informações de entrega
            var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            Order order = _orderService.GetOrderByGuid(processPaymentRequest.OrderGuid);
            checkout.Shipping = new Shipping();
            checkout.Shipping.ShippingType = ShippingType.NotSpecified;
            checkout.Shipping.Cost = order.OrderShippingInclTax;
            checkout.Shipping.Address = new Address(
                _workContext.CurrentCustomer.ShippingAddress.Country.ThreeLetterIsoCode,
                _workContext.CurrentCustomer.ShippingAddress.StateProvince.Abbreviation,
                _workContext.CurrentCustomer.ShippingAddress.City,
                "",
                _workContext.CurrentCustomer.ShippingAddress.ZipPostalCode,
                _workContext.CurrentCustomer.ShippingAddress.Address1,
                _workContext.CurrentCustomer.ShippingAddress.Address1,
                _workContext.CurrentCustomer.ShippingAddress.Address2
            );

            //_pagSeguroPaymentSetting.PagSeguroUrl + @"/transactions"
            //permitir configurar a URL
            var credentials = new AccountCredentials(@_pagSeguroPaymentSetting.PagSeguroEmail, @_pagSeguroPaymentSetting.PagSeguroToken);
            TransactionService.CreateCheckout(credentials, checkout);

            return processPaymentResult;
        }

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //não utilizado
        }

        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart) => 0;

        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest) => new CapturePaymentResult();

        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest) => new RefundPaymentResult();

        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest) => new VoidPaymentResult();

        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest) => new ProcessPaymentResult();

        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest) => new CancelRecurringPaymentResult();

        //Caso a comunicação com o pag seguro falhe, é possivel fazer com que o cliente tente novamente mais tarde sem perder o pedido
        public bool CanRePostProcessPayment(Order order) => false;

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentPagSeguroTransparent";
            routeValues = new RouteValueDictionary()
            {
                { "Namespaces", "NopBrasil.Plugin.Payments.PagSeguroTransparent.Controllers" },
                { "area", null }
            };
        }

        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentPagSeguroTransparent";
            routeValues = new RouteValueDictionary()
            {
                { "Namespaces", "NopBrasil.Plugin.Payments.PagSeguroTransparent.Controllers" },
                { "area", null }
            };
        }

        public Type GetControllerType() => typeof(PaymentPagSeguroTransparentController);

        public bool SupportCapture => false;

        public bool SupportPartiallyRefund => false;

        public bool SupportRefund => false;

        public bool SupportVoid => false;

        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.NotSupported;

        public PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public bool HidePaymentMethod(IList<ShoppingCartItem> cart) => false;


        public string GetPublicViewComponentName()
        {
            return string.Empty;
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentPayPalStandard/Configure";
        }

        public bool SkipPaymentInfo => false;

        public string PaymentMethodDescription => string.Empty;

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var model = new PaymentInfoModel
            {
                CpfTitular = form["CpfTitular"],
                NomeTitular = form["NomeTitular"],
                AnoNascimento = Convert.ToInt32(form["AnoNascimento"]),
                MesNascimento = Convert.ToInt32(form["MesNascimento"]),
                DiaNascimento = Convert.ToInt32(form["DiaNascimento"]),
                NumeroCartao = form["NumeroCartao"],
                CVV = form["CVV"],
                Validade = form["Validade"],
                Banco = form["Banco"],
                DDD = form["DDD"],
                Telefone = form["Telefone"],
                NumeroParcelaId = Convert.ToInt32(form["NumeroParcelaId"]),
                TipoPagamento = form["paymentmethod"],
                SessionId = form["SessionId"],
                Url = form["Url"]
            };

            PaymentInfoValidator<PaymentInfoModel> paymentInfoValidator;
            if (model.TipoPagamento == "CartaoCredito")
            {
                paymentInfoValidator = EngineContext.Current.Resolve<CreditCardInfoValidator>();
            }
            else
            {
                paymentInfoValidator = EngineContext.Current.Resolve<PaymentInfoValidator<PaymentInfoModel>>();
            }
            var validationResult = paymentInfoValidator.Validate(model);
            if (!validationResult.IsValid)
                foreach (var error in validationResult.Errors)
                    warnings.Add(error.ErrorMessage);

            return warnings;
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            //Parse dos dados informados pelo cliente para um objeto ProcessPaymentRequest
            var paymentInfo = new ProcessPaymentRequest();
            paymentInfo.CustomValues.Add("paymentmethod", form["paymentmethod"]);
            paymentInfo.CustomValues.Add("SessionId", form["SessionId"]);

            paymentInfo.CustomValues.Add("IdComprador", form["IdComprador"]);
            paymentInfo.CustomValues.Add("TokenCartao", form["TokenCartao"]);

            if (form["paymentmethod"] == "CartaoCredito")
            {
                paymentInfo.CreditCardNumber = form["NumeroCartao"];
                paymentInfo.CreditCardName = form["NomeTitular"];
                paymentInfo.CreditCardExpireYear = Convert.ToInt32(form["Validade"]);
                paymentInfo.CreditCardExpireMonth = Convert.ToInt32(form["Validade"]);
                paymentInfo.CreditCardCvv2 = form["CVV"];

                paymentInfo.CustomValues.Add("CpfTitular", form["CpfTitular"]);
                paymentInfo.CustomValues.Add("AnoNascimento", form["AnoNascimento"]);
                paymentInfo.CustomValues.Add("MesNascimento", form["MesNascimento"]);
                paymentInfo.CustomValues.Add("DiaNascimento", form["DiaNascimento"]);
                paymentInfo.CustomValues.Add("DDD", form["DDD"]);
                paymentInfo.CustomValues.Add("Telefone", form["Telefone"]);
                paymentInfo.CustomValues.Add("NumeroParcelaId", form["NumeroParcelaId"]);
            }
            else if (form["paymentmethod"] == "Debito")
            {
                paymentInfo.CustomValues.Add("Banco", form["Banco"]);
            }
            else if (form["paymentmethod"] == "Boleto")
            {

            }
            else
            {
                //tipo não identificado, ver o que fazer
            }

            return paymentInfo;
        }
    }
}
