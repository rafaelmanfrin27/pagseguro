﻿var localAmount = 0;
var localMaxInstallmentNoInterest = 0;

var Initializer = {

    start: function (amount, maxInstallmentNoInterest, sessionId, url, wait) {
        $.getScript(url, function () {
            PagSeguroDirectPayment.setSessionId(sessionId);
            PagSeguroDirectPayment.getSenderHash();
        });
        localAmount = amount;
        localMaxInstallmentNoInterest = maxInstallmentNoInterest;
        Initializer.createComponents(wait);

    },

    createComponents: function (textWait) {
        var divNumeroCartao = document.getElementById("NumeroCartaoCredito");
        var componenteRequired = divNumeroCartao.getElementsByClassName("required")[0];
        var spanAguarde = document.createElement("span");
        spanAguarde.setAttribute('id', "numero-cartao-loading-progress");
        spanAguarde.setAttribute('style', "display: none; color:black;");
        spanAguarde.setAttribute('class', "please-wait");
        spanAguarde.innerHTML = textWait;
        componenteRequired.appendChild(spanAguarde);
    }
}

var ViewPaymentMethodController = {

    setVisibleTrueBoleto: function () {
        document.getElementById("boleto").style.display = "block";
        document.getElementById("cartao-credito").style.display = "none";
        document.getElementById("debito").style.display = "none";
    },

    setVisibleTrueCartao: function () {
        document.getElementById("cartao-credito").style.display = "block";
        document.getElementById("boleto").style.display = "none";
        document.getElementById("debito").style.display = "none";
    },

    setVisibleTrueDebito: function () {
        document.getElementById("debito").style.display = "block";
        document.getElementById("cartao-credito").style.display = "none";
        document.getElementById("boleto").style.display = "none";
    },

    registerEvents: function () {
        $('#NumeroCartao').blur(function () {
            ViewPaymentMethodController.buildBrand(this.value);
        });
    },

    buildBrand: function (card) {
        if (card.length >= 6) {
            PagSeguroDirectPayment.getBrand({
                cardBin: card,
                success: function (response) {
                    ViewPaymentMethodController.setCardBrand(response);
                },
                error: function (response) {
                    // Don't Panic 
                    console.log(response.errors[0]);
                },
                complete: function (response) {
                    // Complete !
                }
            });
        }
    },

    setCardBrand: function (response) {
        $("#CVV").attr('maxlength', response.brand.cvvSize);
        ViewPaymentMethodController.buildInstallment(response.brand.name);
    },

    buildInstallment: function (brand) {
        PagSeguroDirectPayment.getInstallments(
		{
		    amount: localAmount,
		    brand: brand.toLowerCase(),
		    maxInstallmentNoInterest: localMaxInstallmentNoInterest,

		    success: function (response) {

		    },
		    error: function (response) {
		        console.log(response.errors[0]);
		    },
		    complete: function (response) {
		    }
		});

        BuilderImageBrand.build(brand);
    }
}


var BuilderImageBrand = {

    build: function (brand) {
        var progressBar = document.getElementById("numero-cartao-loading-progress");
        progressBar.style.display = "inherit";

        PagSeguroDirectPayment.getPaymentMethods({
            success: function (response) {
                BuilderImageBrand.findAndBuildImage(response, brand);
                var progressBar = document.getElementById("numero-cartao-loading-progress");
                progressBar.style.display = "none";
            },
            error: function (response) {
                var progressBar = document.getElementById("numero-cartao-loading-progress");
                progressBar.style.display = "none";
            },
            complete: function (response) {
                var progressBar = document.getElementById("numero-cartao-loading-progress");
                progressBar.style.display = "none";
            }
        });
    },

    findAndBuildImage: function (response, brand) {
        var paymentMethods = response.paymentMethods;
        var options = $.map(paymentMethods.CREDIT_CARD.options, function (value, index) {
            return [value];
        });
        for (var i = options.length - 1; i >= 0; i--) {
            if (options[i].name.toLowerCase() == brand.toLowerCase()) {
                var srcImg = options[i].images.SMALL.path;
                if ((srcImg != "") && (srcImg != undefined)) {
                    var img = document.createElement("img");
                    img.setAttribute('src', 'https://stc.pagseguro.uol.com.br' + srcImg);

                    var divNumeroCartao = document.getElementById("NumeroCartaoCredito");
                    var componenteImg = divNumeroCartao.getElementsByClassName("required")[0];
                    var imgs = componenteImg.getElementsByTagName("img")
                    for (index = imgs.length - 1; index >= 0; index--) {
                        imgs[index].parentNode.removeChild(imgs[index]);
                    }
                    componenteImg.appendChild(img);
                }
            }
        }
    }
}
