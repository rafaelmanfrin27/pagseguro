﻿using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Models;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Services;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Task;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Validators;

namespace NopBrasil.Plugin.Misc.PagSeguroTransparent
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<PaymentInfoValidator<PaymentInfoModel>>().AsSelf().InstancePerDependency();
            builder.RegisterType<CreditCardInfoValidator>().AsSelf().InstancePerDependency();
            builder.RegisterType<PagSeguroService>().As<IPagSeguroService>().InstancePerDependency();
            builder.RegisterType<PagSeguroTask>().AsSelf();
        }

        public int Order => 1;
    }
}
