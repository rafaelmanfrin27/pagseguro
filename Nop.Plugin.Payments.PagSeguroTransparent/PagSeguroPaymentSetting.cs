﻿using Nop.Core.Configuration;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent
{
    public class PagSeguroPaymentSetting : ISettings
    {
        public string PagSeguroEmail { get; set; }
        public string PagSeguroToken { get; set; }
        public string PagSeguroUrl { get; set; }
        public string PagSeguroUrlSTC { get; set; }
        public int MaxInstallmentsNumberInterestFree { get; set; }
        public int Timeout { get; set; }
        public string DescricaoBoleto { get; set; }
        public string DescricaoDebito { get; set; }
        public string CpfAttributeName { get; set; }
    }
}
