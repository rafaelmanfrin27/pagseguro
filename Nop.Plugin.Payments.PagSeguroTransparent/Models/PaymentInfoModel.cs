﻿using System.Collections.Generic;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Validators.Common;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Models
{
    [Validator(typeof(AddressValidator))]
    public class PaymentInfoModel : BaseNopModel
    {
        public PaymentInfoModel()
        {
            NumeroParcelas = new List<SelectListItem>();
            NumeroParcelaId = 0;
        }
        public string SessionId { get; set; }
        public string Url { get; set; }

        public string IdComprador { get; set; }
        public string TokenCartao { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Numero")]
        public string NumeroCartao { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Validade")]
        public string Validade { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.NomeTitular")]
        public string NomeTitular { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.CVV")]
        public string CVV { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.NumeroParcelas")]
        public int NumeroParcelaId { get; set; }
        public IList<SelectListItem> NumeroParcelas { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.CpfTitular")]
        public string CpfTitular { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.Telefone")]
        public string Telefone { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.DDD")]
        public string DDD { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Cartao.DataNascimento")]
        public int DiaNascimento { get; set; }
        public int MesNascimento { get; set; }
        public int AnoNascimento { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Banco")]
        public string Banco { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.TipoPagamento")]
        public string TipoPagamento { get; set; }

        public string DescricaoBoleto { get; set; }

        public string DescricaoDebito { get; set; }

        public int MaxInstallmentsNumberInterestFree { get; set; }

        public decimal Amount { get; set; }
    }
}
