﻿using System;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent
{
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.EmailAdmin.PagSeguro")]
        public String PagSeguroEmail { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Token.PagSeguro")]
        public string PagSeguroToken { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguro")]
        public string PagSeguroUrl { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Url.PagSeguroSTC")]
        public string PagSeguroUrlSTC { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.MaxInstallmentsNumber.InterestFree")]
        public int MaxInstallmentsNumberInterestFree { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Timeout")]
        public int Timeout { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Boleto.Descricao")]
        public string DescricaoBoleto { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.Debito.Descricao")]
        public string DescricaoDebito { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PagSeguroTransparent.Fields.CpfAttributeName")]
        public string CpfAttributeName { get; set; }
    }
}
