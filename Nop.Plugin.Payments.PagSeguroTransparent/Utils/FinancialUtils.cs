﻿namespace NopBrasil.Framework.Utils
{
    public static class FinancialUtils
    {
        public static decimal CalcularValorParcelaFinanciamentoPrice(decimal valorFinanciamento, decimal taxaJuros, int numeroParcelas)
        {
            return valorFinanciamento * (taxaJuros / (1 - MathUtils.Pow(1 + taxaJuros, -numeroParcelas)));
        }

        public static decimal CalcularValorJuros(decimal valorFuturo, decimal valorPresente, int numeroParcelas)
        {
            decimal v1 = valorFuturo / valorPresente;
            decimal v2 = 1.00M / numeroParcelas;
            return MathUtils.Pow(v1, v2) - 1;
        }

        public static decimal CalcularValorFuturo(decimal valorPresente, decimal taxaJuros, int numeroParcelas)
        {
            return valorPresente * (1 + (taxaJuros / 100) * numeroParcelas);
        }

        public static decimal CalcularValorParcela(decimal valorPresente, decimal taxaJuros, int numeroParcelas)
        {
            return CalcularValorFuturo(valorPresente, taxaJuros, numeroParcelas) / numeroParcelas;
        }
    }
}
