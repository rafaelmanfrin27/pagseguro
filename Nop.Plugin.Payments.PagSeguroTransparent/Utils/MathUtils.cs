﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopBrasil.Framework.Utils
{
    public static class MathUtils
    {
        public static decimal Pow(decimal x, decimal y)
        {
            return (decimal)Math.Pow((double)x, (double)y);
        }
    }
}
