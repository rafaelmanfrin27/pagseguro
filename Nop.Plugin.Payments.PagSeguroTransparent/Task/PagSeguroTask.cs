﻿using Microsoft.Build.Framework;
using Nop.Core.Domain.Tasks;
using Nop.Services.Tasks;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Services;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Task
{
    public class PagSeguroTask : ITask
    {
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly IPagSeguroService _pagSeguroService;

        public IBuildEngine BuildEngine { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
        public ITaskHost HostObject { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public PagSeguroTask(IScheduleTaskService scheduleTaskService, IPagSeguroService pagSeguroService)
        {
            this._scheduleTaskService = scheduleTaskService;
            this._pagSeguroService = pagSeguroService;
        }


        private ScheduleTask GetScheduleTask()
        {
            ScheduleTask task = _scheduleTaskService.GetTaskByType(GetTaskType());
            if (task == null)
            {
                task = new ScheduleTask();
                task.Type = GetTaskType();
                task.Name = "PagSeguro - Transparent Checkout";
                task.Enabled = true;
                task.StopOnError = false;
                task.Seconds = 300;
            }
            return task;
        }

        protected string GetTaskType() => "NopBrasil.Plugin.Payments.PagSeguroTransparent.Task.PagSeguroTask, NopBrasil.Plugin.Payments.PagSeguroTransparent";

        public void InstallTask()
        {
            ScheduleTask scheduleTask = GetScheduleTask();
            if (scheduleTask.Id > 0)
                _scheduleTaskService.UpdateTask(scheduleTask);
            else
                _scheduleTaskService.InsertTask(scheduleTask);
            RestartAllTasks();
        }

        public void UninstallTask()
        {
            ScheduleTask scheduleTask = GetScheduleTask();
            if (scheduleTask.Id > 0)
            {
                _scheduleTaskService.DeleteTask(scheduleTask);
                RestartAllTasks();
            }
        }

        private void RestartAllTasks()
        {
            TaskManager.Instance.Stop();
            TaskManager.Instance.Initialize();
            TaskManager.Instance.Start();
        }

         bool ITask.Execute()
        {
            _pagSeguroService.ConfirmarPagamentosPendentes();
            return true;
        }
    }
}
