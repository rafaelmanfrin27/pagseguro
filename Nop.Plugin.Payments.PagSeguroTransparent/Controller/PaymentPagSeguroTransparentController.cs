﻿using System;
using System.Linq;
using Autofac;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Orders;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Models;
using Uol.PagSeguro.Domain;
using Uol.PagSeguro.Service;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent
{
    public class PaymentPagSeguroTransparentController : BasePaymentController
    {
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly PagSeguroPaymentSetting _pagSeguroPaymentSetting;
        private readonly ICacheManager _cacheManager;
        private readonly ILogger _logger;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;

        public PaymentPagSeguroTransparentController(ISettingService settingService, IWebHelper webHelper, PagSeguroPaymentSetting pagSeguroPaymentSetting, ICacheManager cacheManager,
            ILogger logger, IOrderTotalCalculationService orderTotalCalculationService, IWorkContext workContext, ICustomerService customerService, IStoreContext storeContext,
            ICustomerAttributeParser customerAttributeParser, ICustomerAttributeService customerAttributeService)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._pagSeguroPaymentSetting = pagSeguroPaymentSetting;
            this._cacheManager = cacheManager;
            this._logger = logger;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._workContext = workContext;
            this._customerService = customerService;
            this._storeContext = storeContext;
            this._customerAttributeParser = customerAttributeParser;
            this._customerAttributeService = customerAttributeService;
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            var model = new ConfigurationModel();
            model.MaxInstallmentsNumberInterestFree = _pagSeguroPaymentSetting.MaxInstallmentsNumberInterestFree;
            model.PagSeguroEmail = _pagSeguroPaymentSetting.PagSeguroEmail;
            model.PagSeguroToken = _pagSeguroPaymentSetting.PagSeguroToken;
            model.PagSeguroUrl = _pagSeguroPaymentSetting.PagSeguroUrl;
            model.PagSeguroUrlSTC = _pagSeguroPaymentSetting.PagSeguroUrlSTC;
            model.Timeout = _pagSeguroPaymentSetting.Timeout;
            model.DescricaoBoleto = _pagSeguroPaymentSetting.DescricaoBoleto;
            model.DescricaoDebito = _pagSeguroPaymentSetting.DescricaoDebito;
            model.CpfAttributeName = _pagSeguroPaymentSetting.CpfAttributeName;
            return View("~/Plugins/Payments.PagSeguroTransparent/Views/PagSeguroTransparent/Configure.cshtml", model);
        }

        [HttpPost]
        [AuthorizeAdmin]
        [AdminAntiForgery]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();
            try
            {
                _pagSeguroPaymentSetting.MaxInstallmentsNumberInterestFree = model.MaxInstallmentsNumberInterestFree;
                _pagSeguroPaymentSetting.PagSeguroEmail = model.PagSeguroEmail;
                _pagSeguroPaymentSetting.PagSeguroToken = model.PagSeguroToken;
                _pagSeguroPaymentSetting.PagSeguroUrl = model.PagSeguroUrl;
                _pagSeguroPaymentSetting.PagSeguroUrlSTC = model.PagSeguroUrlSTC;
                _pagSeguroPaymentSetting.Timeout = model.Timeout;
                _pagSeguroPaymentSetting.DescricaoBoleto = model.DescricaoBoleto;
                _pagSeguroPaymentSetting.DescricaoDebito = model.DescricaoDebito;
                _pagSeguroPaymentSetting.CpfAttributeName = model.CpfAttributeName;

                _settingService.SaveSetting(_pagSeguroPaymentSetting);
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
            }

            return Configure();
        }
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult PaymentInfo()
        {
            var model = new PaymentInfoModel();

            try
            {
                var credentials = new ApplicationCredentials(_pagSeguroPaymentSetting.PagSeguroUrl + "/sessions", _pagSeguroPaymentSetting.PagSeguroEmail, _pagSeguroPaymentSetting.PagSeguroToken);
                var sessionService = SessionService.CreateSession(credentials);

                model.SessionId = sessionService.id;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
            }

            model.MaxInstallmentsNumberInterestFree = _pagSeguroPaymentSetting.MaxInstallmentsNumberInterestFree;

            var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            model.Amount = _orderTotalCalculationService.GetShoppingCartTotal(cart).Value;
            model.Url = _pagSeguroPaymentSetting.PagSeguroUrlSTC;

            model.NomeTitular = _workContext.CurrentCustomer.Username;

            var xmlAtts = _workContext.CurrentCustomer.BillingAddress.CustomAttributes;
            var attributes = _customerAttributeService.GetAllCustomerAttributes();
            model.CpfTitular = _customerAttributeParser.ParseValues(xmlAtts, attributes.Where(aa => aa.Name == _pagSeguroPaymentSetting.CpfAttributeName).FirstOrDefault().Id)[0];
            model.Telefone = _workContext.CurrentCustomer.BillingAddress.PhoneNumber;

            return View("~/Plugins/Payments.PagSeguroTransparent/Views/PagSeguroTransparent/PaymentInfo.cshtml", model);
        }

    }
}
