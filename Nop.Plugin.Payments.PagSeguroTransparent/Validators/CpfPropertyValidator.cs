﻿using FluentValidation.Validators;
using NopBrasil.Framework.Utils;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Validators
{
    public class CpfPropertyValidator : PropertyValidator
    {
        public CpfPropertyValidator() : base("CPF is not valid")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var cpfCnjpValue = context.PropertyValue as string;
            if (string.IsNullOrWhiteSpace(cpfCnjpValue))
                return false;

            return CpfCnpjUtils.IsValid(cpfCnjpValue);
        }
    }
}
