﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Validators
{
    public static class PagSeguroValidatorExtensions
    {
        public static IRuleBuilderOptions<T, string> IsCpfValid<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new CpfPropertyValidator());
        }
    }
}
