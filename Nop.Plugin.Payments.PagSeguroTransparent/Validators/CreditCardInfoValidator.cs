﻿using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Models;
using FluentValidation;
using Nop.Web.Framework.Validators;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Validators
{
    public class CreditCardInfoValidator : PaymentInfoValidator<PaymentInfoModel>
    {
        public CreditCardInfoValidator(ILocalizationService localizationService) : base(localizationService)
        {
            RuleFor(x => x.AnoNascimento).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.MesNascimento).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.DiaNascimento).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.NumeroCartao).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.CVV).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.Validade).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.NomeTitular).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.NumeroParcelaId).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.CpfTitular).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.DDD).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.Telefone).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.TokenCartao).NotEmpty().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.NumeroCartao).IsCreditCard().WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.CVV).Matches(@"^[0-9]{3,4}$").WithMessage(_localizationService.GetResource(""));
            RuleFor(x => x.CpfTitular).IsCpfValid().WithMessage(_localizationService.GetResource(""));
        }
    }
}
