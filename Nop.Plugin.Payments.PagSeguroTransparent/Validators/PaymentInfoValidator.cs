﻿using FluentValidation;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NopBrasil.Plugin.Payments.PagSeguroTransparent.Models;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Validators
{
    public class PaymentInfoValidator<T> : BaseNopValidator<PaymentInfoModel>
    {
        protected readonly ILocalizationService _localizationService;

        public PaymentInfoValidator(ILocalizationService localizationService)
        {
            this._localizationService = localizationService;
            RuleFor(x => x.IdComprador).NotEmpty().WithMessage(_localizationService.GetResource(""));
        }
    }
}
