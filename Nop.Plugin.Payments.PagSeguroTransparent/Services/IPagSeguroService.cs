﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopBrasil.Plugin.Payments.PagSeguroTransparent.Services
{
    public interface IPagSeguroService
    {
        void ConfirmarPagamentosPendentes();
    }
}
